<?php
require '../../Database/Requete.php';
include('../../config/config.php');
// on se connecte à MySQL et on sélectionne la base //On créé la requête
$addvalue = [
    'prix' => $_POST['prix'],
    'type' => $_POST['type'],
    'dimension' => $_POST['dimension'],
    'capacité' => $_POST['capacité'],
    'image' => $_FILES['image']['name']
];
if (!empty($_POST['prix']) and !preg_match("/ /i", $_POST['prix']) and is_numeric($_POST['prix'])) {

    if (!empty($_POST['dimension']) and is_numeric($_POST['dimension'])) {

        if (!empty($_POST['capacité']) and (($_POST['capacité']) > 0) and is_numeric($_POST['capacité'])) {

            if (isset($_FILES['image']) AND $_FILES['image']['error'] == 0) {
                // Testons si le fichier n'est pas trop gros
                if ($_FILES['image']['size'] <= 10000000) {
                    // Testons si l'extension est autorisée
                    $infosfichier = pathinfo($_FILES['image']['name']);

                    $extension_upload = $infosfichier['extension'];

                    $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');

                    if (in_array($extension_upload, $extensions_autorisees)) {
                        // On peut valider le fichier et le stocker définitivement
                        echo 'l\'envoi a bien été effectué';
                        $bdd1 = new PDO ('mysql:host=localhost:3308;dbname=bdd_pa', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

                        $q1 = "INSERT INTO location(prix, type, dimension, capacité, image) VALUES( :prix, :type, :dimension, :capacité, :image)";
                        $stmt1 = $bdd1->prepare($q1);

                        if (!($result = $stmt1->execute($addvalue))) {
                            foreach ($addvalue as $key => $param) {
                                $stmt1->bindParam($key, $param);
                            }
                            $result = $stmt1->execute($addvalue);
                        }

                        ?>
                        <?php
                        $a = " SELECT prix, type, dimension, capacité, image FROM location ORDER BY prix ASC";
                        $stmt1 = $pdo->prepare($a);
                        $data1 = $stmt1->execute();
                        $result = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                        foreach ($result as $key => $value) : ?>
                            <div class="container">
                                <div class="row">
                                    <div class="column">
                                        <h2 class="locaux"><?= $value['prix']; ?></h2><br>
                                        <h2>type:<?= $value['type']; ?></h2><br>
                                        <h2>dimension:<?= $value['dimension']; ?></h2><br>
                                        <h2>capacité:<?= $value['capacité']; ?></h2><br>
                                        <h2>image:<?= $value['image']; ?></h2>
                                    </div>
                                    <div class="column">
                                        <img class="result" src="../../image/<?= $value['image']; ?>"/>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php
                    } else {

                        echo '<h1>' . ' l envoi a échoué, veuillez vérifié l\'extension votre fichier 
                	 ' . '</h1>';
                    }
                } else {
                    echo "123";
                }
            } else {
                echo "image incorrect";
            }

        } else {
            echo "capacité non validé";
        }

    } else {
        echo "dimension non validé";
    }

} else {
    echo "prix non validé";
}


?>